﻿//Даны четыре неотрицательных числа a, b, c и d. Сложить две рациональные дроби a/b и c/d, а их результат представить в виде несократимой дроби m/n. Вывести числа m и n. a, b, c, d ≤ 1000.
#include <iostream>
#include <algorithm>
std::pair<int, int> sum_of_fractions(int a, int b, int c, int d)
{
	int m = d * a + b * c;
	int n = b * d;
	int mini = std::min(m, n);
	for (int i = 2; i <= mini; i++)
	{
		if (m % i == 0 && n % i == 0)
		{
			m /= i;
			n /= i;
			i = 1;
		}
	}
	std::pair<int, int> answer;
	answer.first = m;
	answer.second = n;
	return answer;
}
int main()
{
	int a = 0, b = 0, c = 0, d = 0;
	std::cin >> a >> b >> c >> d;
	std::pair<int, int> answer;
	answer = sum_of_fractions(a, b, c, d);
	std::cout << answer.first << " " << answer.second;
	return 0;
}
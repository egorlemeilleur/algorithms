﻿/* Для сложения чисел используется старый компьютер. Время, затрачиваемое на нахождение суммы двух чисел равно их сумме.
Таким образом для нахождения суммы чисел 1,2,3 может потребоваться разное время, в зависимости от порядка вычислений.
((1+2)+3) -> 1+2 + 3+3 = 9 ((1+3)+2) -> 1+3 + 4+2 = 10 ((2+3)+1) -> 2+3 + 5+1 = 11
Требуется написать программу, которая определяет минимальное время, достаточное для вычисления суммы заданного набора чисел.
Требуемое время работы O(n*log(n)). */

#include <iostream>
#include <queue>
#include <vector>
int sum_of_computer(std::priority_queue<int, std::vector<int>, std::greater<int>>& heap)
{
    int sum = 0;
    while (heap.size() != 1)
    {
        const int temporary_min_heap_1 = heap.top();
        heap.pop();
        const int temporary_min_heap_2 = heap.top();
        heap.pop();
        int temporary_sum = temporary_min_heap_1 + temporary_min_heap_2; //запоминаем сумму двух минимальных элементов
        sum += temporary_sum;
        heap.push(temporary_sum); //возвращаем сумму в кучу
    }
    return sum;
}
int main()
{
    std::priority_queue<int, std::vector<int>, std::greater<int>> heap; //создаем кучу, в корне которой лежит минимальное число
    int heap_size;
    std::cin >> heap_size;
    for (int i = 0; i < heap_size; i++)
    {
        int number;
        std::cin >> number;
        heap.push(number);
    }
    std::cout << sum_of_computer(heap);
    return 0;
}

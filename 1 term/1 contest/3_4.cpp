﻿//Дан отсортированный массив различных целых чисел A[0..n - 1] и массив целых чисел B[0..m - 1].
//Для каждого элемента массива B[i] найдите минимальный индекс элемента массива A[k], ближайшего по значению к B[i].
//n ≤ 110000, m ≤ 1000.

#include <iostream>
#include <vector>
#include <cmath>
int binary_search(std::vector<int>& array_1, int element) //т.к. первый массив отсортирован, бинарным поиском находим место вставки элемента второго массив в первый массив
{
    int first = -1;
    int last = array_1.size();
    while (first < last - 1)
    {
        int mid = (first + last) / 2;
        if (array_1[mid] < element)
            first = mid;
        else
            last = mid;
    }
    return first + 1;
}
int finding_twins(std::vector<int>& array_1, int element_of_array2)
{
    int position_of_insertion = binary_search(array_1, element_of_array2); //возвращаем позицию вставки
    if (position_of_insertion == 0) return 0;
    else if (position_of_insertion == array_1.size()) return array_1.size() - 1;
    else
    {
        if (abs(array_1[position_of_insertion - 1] - element_of_array2) <= abs(array_1[position_of_insertion] - element_of_array2))
        {
            return position_of_insertion - 1;
        }
        else
        {
            return position_of_insertion;
        }
    }
}
int main()
{
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(NULL);
    std::cout.tie(NULL);
    int n, m;
    std::cin >> n;
    std::vector<int> array_1(n);
    for (int i = 0; i < n; i++)
    {
        std::cin >> array_1[i];
    }
    std::cin >> m;
    for (int i = 0; i < m; i++)
    {
        int element_of_array2 = 0;
        std::cin >> element_of_array2;
        std::cout << finding_twins(array_1, element_of_array2) << " ";
    }
    return 0;
}

﻿/* В круг выстроено N человек, пронумерованных числами от 1 до N. Будем исключать каждого k-ого до тех пор, пока не уцелеет только один человек.
Например, если N=10, k=3, то сначала умрет 3-й, потом 6-й, затем 9-й, затем 2-й, затем 7-й, потом 1-й, потом 8-й, за ним - 5-й, и потом 10-й. Таким образом, уцелеет 4-й.
Необходимо определить номер уцелевшего. 1 ≤ k ≤ N ≤ 10000. */

#include <iostream>  /* стандартная игра: выживший имеет позицию p, раунд начинается с 0 индекса (1 элемент "массива"). Если начать с конца игры, имея двух людей, то победитель имеет позицию 0,
											а в начале игры (когда будет n человек) он будет иметь позицию p */
#include <vector>
int counting_of_survivor(int n, int k) //вычисляем победителя, начиная с последнего раунда до первого  
{
	int survivor = 0; //индекс выжившего человека (победителя) в "массиве" в последнем раунде
	for (int quantity_of_people = 2; quantity_of_people <= n; quantity_of_people++) //количество людей растет до n каждый раунд
	{
		survivor = (survivor + k) % quantity_of_people; //с помощью этой формулы вычисляем начальную позицию каждого раунда (индекс следующего после выбывшего человека) 
	}
	return survivor + 1; //возвращаем +1, так как значения в "массиве" сдвинуты на 1
}
int main()
{
	int n, k;
	std::cin >> n >> k;
	std::cout << counting_of_survivor(n, k);
	return 0;
}
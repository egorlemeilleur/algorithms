﻿#include <iostream>
#include <vector>
#include <queue>
struct Event
{
    // координата
    int time;
    // два типа:
    // начало отрезка: +1
    // конец отрезка: -1
    int type;
    bool operator<(const Event& event) const
    {
        return time < event.time || time == event.time && type > event.type;
    }
};
int calc_painted_length(const std::vector<Event>& borders)
{
    int current_overlay = 0;
    int total_overlay = 0;
    int last_border = 0;
    for (int i = 0; i < borders.size(); i++)
    {
        if (current_overlay == 1)
        {
            total_overlay += borders[i].time - last_border;
        }
        current_overlay += borders[i].type;
        last_border = borders[i].time;
    }
    return total_overlay;
}
void heap_sort(std::vector<Event>& borders)
{
    std::priority_queue <Event, std::vector<Event>> heap;
    for (int i = 0; i < borders.size(); i++)
    {
        heap.push(borders[i]);
    }
    for (int i = borders.size() - 1; i >= 0; i--)
    {
        borders[i] = heap.top();
        heap.pop();
    }
}
void SiftDown(std::vector<Event>& borders, int i) {
    int left = 2 * i + 1;
    int right = 2 * i + 2;
    int largest = i;
    if (left < borders.size() && borders[left] > borders[i])
        largest = left;
    if (right < borders.size() && borders[right] > borders[largest])
        largest = right;
    if (largest != i) {
        std::swap(borders[i], borders[largest]);
        SiftDown(borders, largest);
    }
}
void BuildHeap(std::vector<Event>& borders, int i) {
    for (int i = borders.size() / 2 – 1; i >= 0; --i) {
        SiftDown(borders, i);
    }
}
void HeapSort(std::vector<Event>& borders) {
    int heapSize = borders.size();
    BuildHeap(borders, heapSize);
    while (heapSize > 1) {
        swap(borders[0], borders[heapSize - 1]);
        --heapSize;
        SiftDown(borders, heapSize, 0);
    }
}
int main()
{
    int number_of_segments = 0;
    std::cin >> number_of_segments;

    std::vector<Event> borders(2 * number_of_segments);
    for (int i = 0; i < number_of_segments; i++)
    {
        std::cin >> borders[2 * i].time >> borders[2 * i + 1].time;
        borders[2 * i].type = 1;
        borders[2 * i + 1].type = -1;
    }
    heap_sort(borders);
    std::cout << calc_painted_length(borders);
    return 0;
}